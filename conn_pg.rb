require 'pg'
require 'factory_bot'
require 'faker'

include FactoryBot::Syntax::Methods

FactoryBot.define do
  factory :investor, class: Hash do
    name { Faker::Name.name.gsub(/\'/,' ') }
    email { Faker::Internet.email }
  end

  factory :account, class: Hash do
    iban { Faker::Bank.iban }
  end
end

begin
    # Initialize connection variables.
    host = 'localhost'
    database = 'fides_db'

    # Initialize connection object.
    connection = PG::Connection.new(dbname: 'postgres')
    puts 'Successfully created connection to system database postgres'
    connection.exec("DROP DATABASE IF EXISTS #{database}")
    
    connection.exec("CREATE DATABASE #{database}")
    puts "Successfully created database #{database}"
    connection.close
    puts 'Successfully closed connection to database postgres'

    connection = PG::Connection.new(dbname: database)
    puts "Successfully created connection to database #{database}"

    # Drop previous table of same name if one exists
    connection.exec('DROP TABLE IF EXISTS investors;')
    puts 'Finished dropping table (if existed).' 

    # Create new tables.
    connection.exec('CREATE TABLE investors (
                                             id          SERIAL PRIMARY KEY,
                                             name        VARCHAR(50),
                                             email       VARCHAR(50),
                                             created_at  timestamp DEFAULT current_timestamp
                                             );
                    ')
    connection.exec('CREATE TABLE accounts (
                                            id          SERIAL PRIMARY KEY,
                                            iban        VARCHAR(50),
                                            investor_id INTEGER REFERENCES investors (id),
                                            created_at  timestamp DEFAULT current_timestamp
                                            );
                    ')

    puts 'Finished creating tables.'

    # Insert some data into tables.
    500.times do |id|
      investor = attributes_for(:investor)
      account  = attributes_for(:account)
      connection.exec("INSERT INTO investors VALUES(
                                                    #{id},
                                                    \'#{investor[:name]}\',
                                                    \'#{investor[:email]}\'
                                                    )
                      ")
      connection.exec("INSERT INTO accounts  VALUES(
                                                    DEFAULT,
                                                    \'#{account[:iban]}\', 
                                                    #{id}
                                                    )
                      ")

    end
    puts 'Inserted 500 rows of data.'

rescue PG::Error => e
    puts e.message 
    
ensure
    connection.close if connection
end